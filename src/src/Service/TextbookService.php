<?php
namespace App\Service;

use App\Domain\Entity\Textbook;
use App\Repository\TextbookRepository;
use App\Repository\UserRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Domain\Entity\User;

class TextbookService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var TextbookRepository
     */
    private $textbookRepository;
    private $userRepository;

    private $mailService;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        TextbookRepository $textbookRepository,
        UserRepository $userRepository
    ) {
        $this->textbookRepository = $textbookRepository;
        $this->userRepository = $userRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }
    public function create($request, ?User $user)
    {
        $request = $this->serializer->deserialize($request, 'App\Domain\Entity\Textbook', 'json');

        $textbook = new Textbook();
        $textbook->setName($request->getName());
        if ($user !== null) {
            $textbook->setUser($this->userRepository->find($user));
        }
        $this->validate($textbook);

        return $this->textbookRepository->create($textbook);
    }


    private function validate(Textbook $textbook)
    {
        if ($this->textbookRepository->findBy(['name' => $textbook->getName()])) {
            throw new \Exception('Nazwa jest już zajeta');
        }
        $validationErrors = $this->validator->validate($textbook);
        if (\count($validationErrors) > 0) {
            $errors = [];
            /** @var ConstraintViolation $error */
            foreach ($validationErrors as $error) {
                $errors['error'][] = array(
                    'type' => 'Validation error',
                    'property' => $error->getPropertyPath(),
                    'message' => $error->getMessage()
                );
            }
            throw new \Exception($this->serializer->serialize($errors, 'json'));
        }
    }
}
