<?php
namespace App\Service\Message;

use App\Domain\Entity\User;
use Twig\Environment;

class EmailMessageService
{
    private $twig;

    private $mailer;

    public function __construct(\Swift_Mailer $mailer, Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function successfulRegister($user)
    {
        $sender = 'demo@buksa.pl';
                
        $msg = new \Swift_Message('Nowe konto E-Podręczniki');
        $msg->setFrom($sender);
        $msg->setTo($user->getEmail());
        $msg->setBody(
            $this->twig->render(
                '/Email/register.html.twig',
                [
                    'firstname' => $user->getFirstName(),
                    'company' => 'AGH E-Podręcznik',
                ]
            ),
            'text/html'
        );
        $msg->setCharset('utf-8');
        return $this->mailer->send($msg, $failure);
    }
}