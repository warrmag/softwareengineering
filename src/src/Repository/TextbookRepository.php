<?php
/**
 * Created By Karol Groblicki
 */
namespace App\Repository;

use App\Domain\Entity\Textbook;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Textbook|null find($id, $lockMode = null, $lockVersion = null)
 * @method Textbook|null findOneBy(array $criteria, array $orderBy = null)
 * @method Textbook[]    findAll()
 * @method Textbook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TextbookRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Textbook::class);
    }

    public function create(Textbook $textbook) : ? Textbook
    {
        try {
            $em = $this->getEntityManager();
            $em->persist($textbook);
            $em->flush();

        } catch (\Doctrine\ORM\ORMInvalidArgumentException $invalidArgumentException) {
            $this->logger->error($invalidArgumentException->getMessage());

            throw new Exception($invalidArgumentException);
        } catch (\Doctrine\ORM\ORMException $ORMException) {
            $this->logger->error($ORMException->getMessage());

            throw new Exception($ORMException);
        }
        return $textbook;
    }
    public function update(Textbook $textbook) : ? Textbook
    {
        $em = $this->getEntityManager();
        /** @var Textbook $updatedLocation */
        $updatedLocation = $em->merge($textbook);
        $em->flush($updatedLocation);

        return $updatedLocation;
    }

    public function delete(Textbook $textbook) : ? Textbook
    {
        try {
            $em = $this->getEntityManager();
            $em->remove($textbook);
            $em->flush();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return $textbook;
    }
    // /**
    //  * @return Textbook[] Returns an array of Textbook objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Textbook
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
