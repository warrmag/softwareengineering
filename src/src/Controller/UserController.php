<?php

namespace App\Controller;

use App\Domain\Entity\User;
use App\Service\UserService;
use App\Service\SecurityService;
use App\Repository\UserRepository;
use App\Service\ResponseService;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class UserController extends AbstractController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    private $userService;
    private $responseService;
    private $userRepository;
    private $securityService;
    private $context;

    public function __construct(
        UserService $userService,
        ResponseService $responseService,
        SecurityService $securityService,
        UserRepository $userRepository,
        AuthorizationCheckerInterface $authChecker,
        TokenStorageInterface $tokenStorage, 
        SerializerInterface $serializer)
    {
        $this->userService = $userService;
        $this->responseService = $responseService;
        $this->securityService = $securityService;
        $this->userRepository = $userRepository;
        $this->tokenStorage = $tokenStorage;
        $this->serializer = $serializer;
        $this->authChecker = $authChecker;
        $this->context = SerializationContext::create()->setGroups(['User']);
    }
    
    public function register(Request $request)
    {
        $response = $this->userService->create($request->getContent());

        return $this->responseService->generate($response, 200);
    }

    public function create(Request $request)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        $response = $this->userService->create($request->getContent());

        return $this->responseService->generateBy($response, $this->context, 201);
    }

    public function self()
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (null === $user) {
            throw new NotFoundHttpException('Nie odnaleziono użytkownika',null,404);
        }
        if (!$user->getIsActive()) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }

        return $this->responseService->generate($user, 200);
    }

    public function show(User $user = null)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        if (null === $user) {
            throw new NotFoundHttpException('Nie odnaleziono użytkownika',null,404);
        }
        return $this->responseService->generate($user, 200);
    }

    public function showAll()
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }

        $users = $this->userRepository->findAll();
        if ($users === null) {
            throw new NotFoundHttpException('Nie odnaleziono użytkownika',null,204);
        }

        return $this->responseService->generate($users, 200);
    }

    public function findBy(Request $request)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        $result = $this->userService->findBy(json_decode($request->getContent(),true));

        return $this->responseService->generate($result, 200);
    }

    public function showByRole(string $role)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }

        $users = $this->userRepository->findByRole($role);

        return $this->responseService->generateBy($users, $this->context, 200);
    }

    public function toggleStatus(Request $request)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        $user = $this->userService->setStatus($request->getContent());

        return $this->responseService->generate($user, 200);
    }

    public function update(User $user = null, Request $request)
    {
        if (null === $user) {
            throw new NotFoundHttpException('Nie odnaleziono użytkownika',null,404);
        }
        $user = $this->userService->update($request->getContent(), $user);

        return $this->responseService->generate($user, 200);
    }

    public function resetPassword(Request $request)
    {
        $user = $this->userService->update($request->getContent());

        return $this->responseService->generate($user, 200);
    }

    public function changePassword(Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        if (false === $this->securityService->userAccessPermission($user->getId())) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        $response = $this->userService->changePassword($request->getContent());

        return $this->responseService->generate($response, 200);
    }

    public function changeRoles(Request $request)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }

        $user = $this->userService->updateRoles($request->getContent());
        return $this->responseService->generate($user, 200);
    }
    public function delete(User $user)
    {
        if (false === $this->authChecker->isGranted('ROLE_ADMIN')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        if (null === $user) {
            throw new NotFoundHttpException('Brak wyników',null,404);
        } else if (true === $this->securityService->isAdmin($user)) {
            throw new AccessDeniedHttpException('Nie możesz usunąć tego użytkownika', null, 403);
        }
        $user = $this->userRepository->delete($user);
        return $this->responseService->generate($user, 200);   
    }
}

