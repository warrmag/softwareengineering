<?php
namespace App\Service;

use App\Domain\Entity\User;
use App\Domain\Data\UserUpdateData;
use App\Repository\UserRepository;
use App\Service\Message\EmailMessageService;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    private $mailService;

    public function __construct(
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder,
        ValidatorInterface $validator,
        EmailMessageService $mailService,
        SerializerInterface $serializer)
    {
        $this->userRepository = $userRepository;
        $this->passwordEncoder = $passwordEncoder;
        $this->serializer = $serializer;
        $this->mailService = $mailService;
        $this->validator = $validator;
    }
    public function create($request, $confirmation = true)
    {
        if (is_string($request)) {
            $request = $this->serializer->deserialize($request, 'App\Domain\Data\UserCreateData', 'json');
            $user = new User();
            $user->setUsername($request->getUsername());
            $user->setFirstname($request->getFirstname());
            $user->setLastname($request->getLastName());
            $user->setEmail($request->getEmail());
        
        } else if($request instanceof User){
            $user = $request;
        }
        $user->setPassword($this->passwordEncoder->encodePassword($user, $request->getPassword()));

        $this->validate($user);

        $user = $this->userRepository->create($user);
        if ($user !== null && $confirmation == true) {
            $this->mailService->successfulRegister($user);
        }        
        return $user;
    }

    public function update($request, User $user)
    {
        $updatedUser = $this->serializer->deserialize($request, 'App\Domain\Entity\User', 'json');

        if ($updatedUser->getUsername() !== null) {
            $user->setUsername($updatedUser->getUsername());
        }
        if ($updatedUser->getPassword() !== null) {
            $user->setPassword($this->passwordEncoder->encodePassword($user,$updatedUser->getPassword()));
        }

        if ($updatedUser->getFirstname() !== null) {
            $user->setFirstname($updatedUser->getFirstname());
        }

        if ($updatedUser->getLastname() !== null) {
            $user->setLastname($updatedUser->getLastname());
        }
        if ($updatedUser->getEmail() !== null) {
            $user->setEmail($updatedUser->getEmail());
        }
        if ($updatedUser->getRoles() !== null) {
            $user->setRoles($updatedUser->getRoles());
        }

        $this->validate($user);

        return $this->userRepository->update($user);
    }
    public function setStatus($request)
    {
        $updatedUser = $this->serializer->deserialize($request, 'App\Domain\Entity\User', 'json');

        $user = $this->userRepository->find($updatedUser->getId());

        if ($updatedUser->getId() !== $user->getId()){
            throw new Exception('Nieprawidłowy użytkownik');
        }
        if ($updatedUser->getIsActive() !== null) {
            $user->setIsActive($updatedUser->getIsActive());
        }
        return $this->userRepository->update($user);
    }
    public function changePassword($request)
    {
        $requestUser = $this->serializer->deserialize($request, 'App\Domain\Data\UserModifyData', 'json');
        $user = $this->userRepository->find($requestUser->getId());

        if ($requestUser->getPassword() === null) {
            throw new Exception('Hasło nie może być puste');
        }
        if ($this->passwordEncoder->isPasswordValid($user,$requestUser->getPassword())) {
            throw new Exception('Nowe hasło musi być różne od starego');
        }
        $user->setPassword($this->passwordEncoder->encodePassword($user,$requestUser->getPassword()));

        $this->validate($user);

        return $this->userRepository->update($user);
    }

    public function updateRoles($request)
    {
        $jsonUser = $this->serializer->deserialize($request, 'App\Domain\Entity\User', 'json');
        $user = $this->userRepository->find($jsonUser->getId());

        if ($jsonUser->getId() !== $user->getId()){
            throw new Exception('Nieprawidłowy użytkownik');
        }
        if ($jsonUser->getRoles() !== null) {
            $user->setRoles($jsonUser->getRoles());
        }
        return $this->userRepository->update($user);
    }

    public function findBy($request)
    {
        if ($request === null) {
            throw new Exception('Request cannot be empty');
        }
        
        $result = $this->userRepository->findBy($request);
        
        if ($result === null) {
            throw new Exception('Brak wyników');
        }

        return $result;
    }

    public function validate($user)
    {
        $validationErrors = $this->validator->validate($user);

        if (\count($validationErrors) > 0) {
            $errors = [];
            /** @var ConstraintViolation $error */
            foreach ($validationErrors as $error) {
                $errors['error'][] = array(
                    'type' => 'Validation error',
                    'property' => $error->getPropertyPath(),
                    'message' => $error->getMessage());
            }
            throw new Exception($this->serializer->serialize($errors,'json'));
        }
    }
}
