<?php
namespace App\Service;

use App\Domain\Entity\Chapter;
use App\Domain\Entity\Textbook;
use App\Domain\Entity\User;
use App\Repository\ChapterRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Repository\TextbookRepository;

class ChapterService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var ChapterRepository
     */
    private $chapterRepository;
    private $textbookRepository;

    private $mailService;

    public function __construct(
        ValidatorInterface $validator,
        SerializerInterface $serializer,
        ChapterRepository $chapterRepository,
        TextbookRepository $textbookRepository
    ) {
        $this->chapterRepository = $chapterRepository;
        $this->textbookRepository = $textbookRepository;
        $this->serializer = $serializer;
        $this->validator = $validator;
    }
    public function create($request, ?Textbook $textbook, ?User $user)
    {
        $textbook = $this->textbookRepository->find($textbook);

        $request = $this->serializer->deserialize($request, 'App\Domain\Entity\Chapter', 'json');

        $chapter = new Chapter();
        $chapter->setName($request->getName());
        if ($textbook !== null) {
            $chapter->setTextbook($textbook);
        }
        if ($request->getContent() !== null) {
            $chapter->setContent($request->getContent());
        }
        if ($user !== null) {
            $chapter->setAuthor($user);
        }
        if ($request->getListOrder() !== null) {
            $chapter->setListOrder($request->getListOrder());
        } else {
            $chapter->setListOrder('1');
        }
        $chapter->setTextbook($textbook);
        $this->validate($chapter);

        return $this->chapterRepository->create($chapter);
    }
    public function update($request, Chapter $Chapter)
    {
        $request = $this->serializer->deserialize($request, 'App\Domain\Entity\Chapter', 'json');

        $chapter = $this->chapterRepository->find($chapter);
        if ($request->getName() !== null) {
            $chapter->setName($request->getName());
        }
        if ($chapter->getTextbook() !== null) {
            $chapter->setTextbook($chapter->getTextbook());
        }
        if ($request->getContent() !== null) {
            $chapter->setContent($request->getContent());
        }
        if ($request->getListOrder() !== null) {
            $chapter->setListOrder($request->getListOrder());
        }
        $this->validate($chapter);
        
        return $this->chapterRepository->create($chapter);
    }

    private function validate(Chapter $chapter)
    {
        $validationErrors = $this->validator->validate($chapter);
        if (\count($validationErrors) > 0) {
            $errors = [];
            /** @var ConstraintViolation $error */
            foreach ($validationErrors as $error) {
                $errors['error'][] = array(
                    'type' => 'Validation error',
                    'property' => $error->getPropertyPath(),
                    'message' => $error->getMessage(),
                );
            }
            throw new \Exception($this->serializer->serialize($errors, 'json'));
        }
    }
}
