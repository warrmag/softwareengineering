<?php
namespace App\Tests\Controller;

use App\Domain\Entity\User;
use JMS\Serializer\SerializerBuilder;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    private $client = null;
    private $serializer;

    public function setUp()
    {
        $this->client = static::createClient(array(), array(
            'PHP_AUTH_USER' => 'user',
            'PHP_AUTH_PW' => 'password',
        ));
        $this->serializer = \JMS\Serializer\SerializerBuilder::create()->build();

    }
    
    public function testGetAllUsers()
    {
        $this->client->request('GET', '/user/');
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}