<?php

declare(strict_types=1);

namespace App\Query;

class FilterQuery
{
    public function generateEntityFilter(string $type, string $criteria): array
    {   
        $search = ['Entity' => ucfirst(str_replace('teacher', 'user', $type))];
        $search['Column'] = strtolower(str_replace('student', 'additionalStudents', $type));

        if ($type[-1] == 's') {
            $search['Entity'] = substr($type,0, -1);
            $search['Criteria'] = strtoupper(str_replace('_',' ',$criteria));
        } else {
            if ($criteria == 'member_of') {
                $search['Criteria'] = '=';
            } else if ($criteria == 'not_member_of') {
                $search['Criteria'] = '!=';
            }
        }
        return $search;
    }

} 