<?php
namespace App\Service;

use App\Domain\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SecurityService
{
    
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function __construct(
        TokenStorageInterface $tokenStorage, 
        AuthorizationCheckerInterface $authChecker,
        UserRepository $userRepository)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authChecker = $authChecker;
        $this->userRepository = $userRepository;
    }

    public function userAccessPermission($id = null)
    {
        $token = $this->tokenStorage->getToken();
        $user = $token->getUser();

        if (\in_array('ROLE_ADMIN', $user->getRoles(), true)) {
            return true;
        } else if ($user->getId() == $id) {
            return true;
        }
        return false;
    }

    public function getToken()
    {
        return $token = $this->tokenStorage->getToken();
    }

    public function isAdmin(User $user)
    {
        if (\in_array('ROLE_ADMIN', $user->getRoles(), true)) {
            return true;
        }
        return false;
    }

    public function hasRole(User $user, string $role)
    {
        if (\in_array($role, $user->getRoles(), true)) {
            return true;
        }
        return false;
    }
}