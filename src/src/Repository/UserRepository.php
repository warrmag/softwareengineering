<?php

namespace App\Repository;

use App\Domain\Entity\User;
use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Psr\Log\LoggerInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param RegistryInterface $registry
     * @param LoggerInterface $logger
     */
    public function __construct(RegistryInterface $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, User::class);
        $this->logger = $logger;
    }

    public function create(User $user): ?User
    {
        try {
            $em = $this->getEntityManager();
            $em->persist($user);
            $em->flush();
        } catch (\Doctrine\ORM\ORMInvalidArgumentException $invalidArgumentException) {
            $this->logger->error($invalidArgumentException->getMessage());

            throw new Exception($invalidArgumentException);
        } catch (\Doctrine\ORM\ORMException $ORMException) {
            $this->logger->error($ORMException->getMessage());
            
            throw new Exception($ORMException);
        }

        return $user;
    }
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getAll() 
    {
        try {
            $em = $this->getEntityManager();
            $users = $em->getRepository(User::class)->findAll();
        } catch (\Doctrine\ORM\ORMInvalidArgumentException $invalidArgumentException) {

            return null;
        } catch (\Doctrine\ORM\ORMException $ORMException) {

            return null;
        }        
        return $users;
    }

    public function findByRole($role)
    {
        return $this->createQueryBuilder('u')
        ->where('u.roles LIKE :roles')
        ->setParameter('roles', '%"'.$role.'"%')
        ->getQuery()
        ->getResult();
    }

    public function update(User $user): ?User
    {
        $em = $this->getEntityManager();
        /** @var User $updatedUser */
        $updatedUser = $em->merge($user);
        $em->flush($updatedUser);

        return $updatedUser;
    }

    public function delete(User $user): ?User
    {
        try{
            $em = $this->getEntityManager();
            $em->remove($user);
            $em->flush();
        }
        catch(\Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException $e )
        {
            throw new \Exception('Użytkownik posiada zależności między studentami lub przedmiotami');
        }
        catch(\Doctrine\ORM\ORMException $e )
        {
            throw new \Exception($e->getMessage());
        }
        return $user;
    }

//    /**
//     * @return User[] Returns an array of User objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
