<?php

namespace App\Repository;

use App\Domain\Entity\Chapter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Chapter|null find($id, $lockMode = null, $lockVersion = null)
 * @method Chapter|null findOneBy(array $criteria, array $orderBy = null)
 * @method Chapter[]    findAll()
 * @method Chapter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ChapterRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Chapter::class);
    }

    public function create(Chapter $chapter) : ? Chapter
    {
        try {
            $em = $this->getEntityManager();
            $em->persist($chapter);
            $em->flush();

        } catch (\Doctrine\ORM\ORMInvalidArgumentException $invalidArgumentException) {
            $this->logger->error($invalidArgumentException->getMessage());

            throw new Exception($invalidArgumentException);
        } catch (\Doctrine\ORM\ORMException $ORMException) {
            $this->logger->error($ORMException->getMessage());

            throw new Exception($ORMException);
        }
        return $chapter;
    }
    public function update(Chapter $chapter) : ? Chapter
    {
        $em = $this->getEntityManager();
        /** @var Chapter $updatedLocation */
        $updatedLocation = $em->merge($chapter);
        $em->flush($updatedLocation);

        return $updatedLocation;
    }

    public function delete(Chapter $chapter) : ? Chapter
    {
        try {
            $em = $this->getEntityManager();
            $em->remove($chapter);
            $em->flush();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return $chapter;
    }
    // /**
    //  * @return Chapter[] Returns an array of Chapter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Chapter
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
