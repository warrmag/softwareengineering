<?php
namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ExceptionListener
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getException();
        $response = new Response();
        // var_dump($exception);
        // die();
        $response->setContent(json_encode($exception->getMessage()));
        $response->headers->set('Content-Type', 'application/json');
        if ($exception->getCode() == 0) {
            $response->setStatusCode(400);
        } else {
            $response->setStatusCode($exception->getStatusCode());
        }
        // sends the modified response object to the event
        $event->setResponse($response);
    }
}