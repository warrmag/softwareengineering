## Start up repository

1. Check folder read/write permission
1.5. Get composer! https://getcomposer.org/
2. in src folder use ```composer install``` 
3. check .env file for congfiguration to your database according to docker .env configuration (example below) (.env powinien być w repozytorium
4. ```php bin/console doctrine:database:create``` (jeżeli powie że jest utworzona idź dalej )
5. ```php bin/console doctrine:schema:update --force```
6. Get Docker! https://docs.docker.com/docker-for-windows/install/
7. in main folder use ```sudo docker-compose up -d``` it will build docker containers
8. Everything should be working now
