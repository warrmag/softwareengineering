<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;

class ResponseService
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }
    
    public function generate($data, $status)
    {
        return new JsonResponse($this->serializer->serialize($data,'json'), $status);   
    }

    public function status($data)
    {
        if ($data) {
            return new JsonResponse($this->serializer->serialize($data,'json'), 200);   
        }
        return new JsonResponse($this->serializer->serialize($data,'json'), 503);   
    }

    public function generateBy($data, $context, $status)
    {
        return new JsonResponse($this->serializer->serialize($data, 'json', $context), $status);   
    }
}