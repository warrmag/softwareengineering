<?php

namespace App\Controller;

use App\Domain\Entity\User;
use App\Service\ResponseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class SecurityController extends AbstractController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    private $responseService;

    public function __construct(TokenStorageInterface $tokenStorage, 
    AuthorizationCheckerInterface $authChecker,
    ResponseService $responseService)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authChecker = $authChecker;
        $this->responseService = $responseService;
    }

    public function login()
    {
        if (false === $this->authChecker->isGranted('ROLE_USER')) {
            throw new AccessDeniedHttpException('Brak uprawnień do wykonania tej akcji', null, 401);
        }
        $token = $this->tokenStorage->getToken();
        /** @var User $user */
        $user = $token->getUser();
        
        return $this->responseService->generate($user, 200);
    }
}
