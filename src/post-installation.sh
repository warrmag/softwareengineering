#!/bin/bash
~/composer.phar install -n -q --optimize-autoloader
php bin/console --quiet doctrine:migration:diff 
php bin/console  --no-interaction --allow-no-migration --quiet doctrine:migration:migrate
rm -R ./var/cache 
php bin/console cache:clear --env=prod --no-debug --quiet