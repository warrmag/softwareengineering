<?php

namespace App\Controller;

use App\Domain\Entity\Chapter;
use App\Domain\Entity\Textbook;
use App\Repository\ChapterRepository;
use App\Service\ChapterService;
use App\Service\ResponseService;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class ChapterController extends AbstractController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    private $chapterService;
    private $chapterRepository;
    private $responseService;
    private $userRepository;
    private $securityService;
    private $context;

    public function __construct(
        ResponseService $responseService,
        AuthorizationCheckerInterface $authChecker,
        ChapterService $chapterService,
        ChapterRepository $chapterRepository,
        TokenStorageInterface $tokenStorage,
        SerializerInterface $serializer
    ) {
        $this->responseService = $responseService;
        $this->chapterService = $chapterService;
        $this->chapterRepository = $chapterRepository;
        $this->tokenStorage = $tokenStorage;
        $this->serializer = $serializer;
        $this->authChecker = $authChecker;
        $this->context = SerializationContext::create()->setGroups(['ChapterBasic']);
    }

    public function create(Textbook $textbook = null, Request $request)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (is_string($user)) {
            $user = null;
        }
        $response = $this->chapterService->create($request->getContent(), $textbook, $user);

        return $this->responseService->generate($response, 200);
    }

    public function update($chapter = null, Request $request)
    {
        $chapter = $this->chapterRepository->find($chapter);
        if ($chapter === null) {
            throw new NotFoundHttpException('Nie odnaleziono rozdzialu', null, 404);
        }
        $response = $this->chapterService->update($request->getContent(), $chapter);

        return $this->responseService->generateBy($response, $this->context, 200);
    }

    public function show($chapter = null)
    {
        $chapter = $this->chapterRepository->find($chapter);
        if ($chapter === null) {
            throw new NotFoundHttpException('Nie odnaleziono rozdzialu', null, 404);
        }
        return $this->responseService->generateBy($chapter, $this->context, 201);
    }

    public function showAll()
    {
        $chapters = $this->chapterRepository->findAll();
        if (null === $chapters) {
            throw new NotFoundHttpException('Brak wyników', null, 204);
        }
        return $this->responseService->generateBy($chapters, $this->context, 201);
    }

    public function delete(Chapter $chapter = null)
    {
        if (false === $this->authChecker->isGranted('IS_AUTHENTICATED_ANONYMOUSLY')) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        if (null === $chapter) {
            throw new NotFoundHttpException('Brak wyników', null, 404);
        }
        $chapter = $this->chapterRepository->delete($chapter);
        return $this->responseService->generate($chapter, 200);
    }

}
