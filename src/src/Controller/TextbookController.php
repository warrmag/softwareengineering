<?php

namespace App\Controller;

use App\Service\ResponseService;
use App\Service\TextbookService;
use App\Repository\TextbookRepository;
use App\Domain\Entity\Textbook;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class TextbookController extends AbstractController
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    private $textbookService;
    private $textbookRepository;
    private $responseService;
    private $userRepository;
    private $securityService;
    private $context;

    public function __construct(
        ResponseService $responseService,
        AuthorizationCheckerInterface $authChecker,
        TextbookService $textbookService,
        TextbookRepository $textbookRepository,
        TokenStorageInterface $tokenStorage,
        SerializerInterface $serializer
    ) {
        $this->responseService = $responseService;
        $this->textbookService = $textbookService;
        $this->textbookRepository = $textbookRepository;
        $this->tokenStorage = $tokenStorage;
        $this->serializer = $serializer;
        $this->authChecker = $authChecker;
        $this->context = SerializationContext::create()->setGroups(['User']);
    }

    public function create(Request $request)
    {
        if ($this->authChecker->isGranted('ROLE_USER')) {
            $user = $this->tokenStorage->getToken()->getUser();
        } else {
            $user = null;
        }

        $response = $this->textbookService->create($request->getContent(), $user);

        return $this->responseService->generate($response, 200);
    }


    public function show(Textbook $textbook = null)
    {
        if ($textbook === null) {
            throw new NotFoundHttpException('Nie odnaleziono podręcznika', null, 404);
        }
        return $this->responseService->generate($textbook, 201);

    }

    public function showAll()
    {
        $textbooks = $this->textbookRepository->findAll();
        if (null === $textbooks) {
            throw new NotFoundHttpException('Brak wyników', null, 204);
        }
        return $this->responseService->generateBy($textbooks, 201);
    }


    public function delete(Textbook $textbook = null)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        if (false === $this->authChecker->isGranted('ROLE_SUPER_USER') || $textbook->getAuthor() == $user) {
            throw new AccessDeniedHttpException('Nie posiadasz uprawnień do wykonania tej akcji', null, 403);
        }
        if (null === $textbook) {
            throw new NotFoundHttpException('Brak wyników', null, 404);
        }
        $textbook = $this->textbookRepository->delete($textbook);
        return $this->responseService->generate($textbook, 200);
    }

}